#include <stdio.h>


/**
 * Setup the game from terminal inputs.
 * @param maxScore
 * @return
 */
int start( int* maxScore) {
    printf("Vitejte ve hre PONG!\n");
    printf("Do kolika bodů chcete hrát: \n");
    scanf("%d", maxScore);
    printf("Jste pripraveni hrat!\n");
    return 0;
}
