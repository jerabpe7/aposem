//
// Created by Petr Jeřábek on 23.05.2024.
//

#ifndef APOSEM_WRITER_H
#define APOSEM_WRITER_H

#endif //APOSEM_WRITER_H
#include "font_types.h"


unsigned int hsv2rgb_lcd(int hue, int saturation, int value);
int char_width(int ch, font_descriptor_t *fdes);
void draw_pixel(int x, int y, unsigned short color, unsigned short* fb);
void draw_pixel_big(int x, int y, unsigned short color, unsigned short fb, int scale);
void draw_char(int x, int y, char ch, unsigned short color, font_descriptor_t *fdes, unsigned short fb, int scale);