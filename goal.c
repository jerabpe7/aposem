//
// Created by Petr Jeřábek on 16.05.2024.
//
#define POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include <time.h>


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "writer.h"

/**
 * This method is used when any player scores a goal.
 * It sets the background of the lcd panel to blank and shows who just scored and the current score.
 * @param player
 * @param p1score
 * @param p2score
 * @param mem_base
 * @param parlcd_mem_base
 */
void goal(int player, int p1score, int p2score, unsigned char *mem_base, unsigned char *parlcd_mem_base){
    unsigned short fb[320*480*2];
    font_descriptor_t *fdes;
    int ptr;
    fdes = &font_winFreeSystem14x16;
    for (ptr = 0; ptr < 320*480 ; ptr++) {
        fb[ptr]=0;
    }
    float x=100;
    float y=100;
    char string [9];
    if(player == 1){
        strcpy(string, "Player 1");
    }   else{
        strcpy(string, "Player 2");
    }
    int lengthString1 = sizeof(string) - 1;


    for (int i = 0; i < lengthString1; i++)  {
        char ch = string[i];
        unsigned int col=hsv2rgb_lcd(255, 255, 255);
        if(ch == ' ') {
            x+= 30;
            continue;
        }
        draw_char((int)x,(int)y, ch, col, fdes, *fb, 6);
        if(ch == 'i' || ch =='l'){
            x+= 20;
        }
        else{
            x+=50;
        }
        parlcd_write_cmd(parlcd_mem_base, 0x2c);
    }
    char string2 [] = "scored!";
    int lengthString2 = 7;
    x = 100;
    y += 100;
    for (int i = 0; i < lengthString2; i++)  {
        char ch=string2[i];
        unsigned int col=hsv2rgb_lcd(255,255,255);
        draw_char((int)x,(int)y, ch, col, fdes, *fb, 6);
        if(ch == 'i' || ch =='l'){
            x+= 20;
        }   else{
            x+=50;
        }
        parlcd_write_cmd(parlcd_mem_base, 0x2c);
    }
    char string3[6];
    sprintf(string3, "%d : %d", p1score, p2score);
    int lengthString3 = 5;
    x = 270;
    y = 10;
    for (int i = 0; i < lengthString3; i++)  {
        char ch=string3[i];
        unsigned int col=hsv2rgb_lcd(255,255,255);
        draw_char((int)x,(int)y, ch, col, fdes, *fb, 3);
        if(ch == ' '){
            x+= 10;
            continue;
        }   else{
            x+=25;
        }
        parlcd_write_cmd(parlcd_mem_base, 0x2c);
    }

    for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }


    for(int i = 0; i < 6; i++){
        if(i%2==0){
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF1500;
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = (int32_t)0;


        }else{
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = (int32_t)0;
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = (int32_t)0;
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF1500;

        }
        usleep(1000*150);
    }
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = (int32_t)0;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = (int32_t)0;



}