//
// Created by Petr Jeřábek on 16.05.2024.
//
#include <stdio.h>
#include <string.h>



#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "font_prop14x16.c"
#include "font_rom8x16.c"
#include "writer.h"


/**
 * This method is used when any player has reached the maximum pre-set score.
 * It sets the background lcd to blank and shows the winner.
 * @param player
 * @param parlcd_mem_base
 * @param mem_base
 * @return
 */
int end(int player, unsigned char *parlcd_mem_base, unsigned char *mem_base) {
    unsigned short fb[320*480*2];
    font_descriptor_t *fdes;
    int ptr;


    fdes = &font_winFreeSystem14x16;
    for (ptr = 0; ptr < 320*480 ; ptr++) {
        fb[ptr]=0;
    }
    float x=100;
    float y=50;
    char string [9];
    if(player == 1){
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF1500;
        strcpy(string, "Player 1");
    }   else{
        strcpy(string, "Player 2");
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF1500;
    }

    int lengthString1 = sizeof(string) / sizeof(string[0]) - 1;

    for (int i = 0; i < lengthString1; i++)  {
        char ch=string[i];
        unsigned int col=hsv2rgb_lcd(255,255,255);
        if(ch == ' ') {
            x+= 30;
            continue;
        }
        draw_char((int)x,(int)y, ch, col, fdes, *fb, 6);
        if(ch == 'i' || ch =='l'){
            x+= 20;
        }
        else{
            x+=50;
        }
        parlcd_write_cmd(parlcd_mem_base, 0x2c);
    }
    char string2 [] = "won!";
    int lengthString2 = sizeof(string2) / sizeof(string2[0]) - 1;
    x = 200;
    y+= 100;
    for (int i = 0; i < lengthString2; i++)  {
        char ch=string2[i];
        unsigned int col=hsv2rgb_lcd(255,255,255);
        draw_char((int)x,(int)y, ch, col, fdes, *fb, 6);
        if(ch == 'i' || ch =='l'){
            x+= 20;
        }   else{
            x+=50;
        }
        parlcd_write_cmd(parlcd_mem_base, 0x2c);
    }

    for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }

    return 0;
}