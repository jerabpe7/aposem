# Documentation & Tutorial

## Documentation 
- Because of that I am not able to set up doxygen, so I have to write all functions bellow:


- int movePlayer1(int playerPosition, int playerLastPosition, Player* p);
  - This method is used to move player 1 when an interaction with the knobs is detected. 
- int movePlayer2(int playerPosition, int playerLastPosition, Player* p);
  - This method is used to move player 2 when an interaction with the knobs is detected.
- void renderBoard(Board* b);
  - This method is used to render a separate game background - the walls.
- void renderPlayer(Board *b, Player p);
  - This method is used to render the player according to his position.
- void renderBall(Board *b, Ball ball);
  - This method is used to render the ball according to its position.
- void playerMoveUp(Player* p);
  - This method is used to move the player up.
- void playerMoveDown(Player* p);
  - This method is used to move the player down.
- void renderAll(Board *board, Ball ball, Player player1, Player player2);
  - This method is used to run all renders via one function.
- void pickFirstVector(Ball* ball);
  - This method is used for the picking the first vector of the ball on the start of any round.
- void moveBall(Ball* ball);
  - This method moves the ball to the directions of the ball`s vectors.
- int checkBall(Ball* ball, Player* player1, Player* player2);
  - This method checks the position of the ball to see if it is in a restricted area - the wall, players, the goal zone. 
  - If it is, it either performs a vector switch or adds a point to the player's score and triggers a new round.
- void initializeStartSett(Board* board, Ball* ball, Player* player1, Player* player2);
  - This method is used at the beginning of each round to reset the basic positions and settings for the ball and players.
- int end(int player, unsigned char *parlcd_mem_base, unsigned char *mem_base);
  - This method is used when any player has reached the maximum pre-set score.
  - It sets the background lcd to blank and shows the winner.
- void goal(int player, int p1score, int p2score, unsigned char *mem_base, unsigned char *parlcd_mem_base)
  - This method is used when any player scores a goal. 
  - It sets the background of the lcd panel to blank and shows who just scored and the current score.
- int start( int* maxScore);
  - Sets up the game from terminal inputs.

## Tutorial 
- The game is playable via pongGame.c followed by entering the maximum score in the terminal.
