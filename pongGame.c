#define POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>

#include "start.c"
#include "end.c"
#include "goal.c"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include <time.h>
#include "writer.h"

#define ROWS 32
#define COLS 48
#define BALL_WIDTH 1
#define PLAYER_WIDTH 1
#define PLAYER_HEIGHT 5

typedef struct{
    int positionX;
    int positionY;
    int score;
}Player;


typedef struct{
    int positionX;
    int positionY;
    int vectorX;
    int vectorY;
    int counterOfVectorY;
}Ball;

typedef struct{
    int width;
    int height;
    char board[ROWS][COLS];
    int maxScore;
}Board;

int movePlayer1(int playerPosition, int playerLastPosition, Player* p);
int movePlayer2(int playerPosition, int playerLastPosition, Player* p);
void renderBoard(Board* b);
void renderPlayer(Board *b, Player p);
void renderBall(Board *b, Ball ball);
void playerMoveUp(Player* p);
void playerMoveDown(Player* p);
void renderAll(Board *board, Ball ball, Player player1, Player player2);
void pickFirstVector(Ball* ball);
void moveBall(Ball* ball);
int checkBall(Ball* ball, Player* player1, Player* player2);
void initializeStartSett(Board* board, Ball* ball, Player* player1, Player* player2);

int main(int argc, char *argv[]) {
    Ball ball;
    Player player1 = {.score = 0};
    Player player2 = {.score = 0};
    Board board;
    initializeStartSett(&board, &ball, &player1, &player2);
    renderAll(&board, ball, player1, player2);

    start(&board.maxScore);

//    int counter = 0;
//    int newRound = 0;
//    bool up;
//    while(1){
//        if (newRound == 1) {
//            newRound = 0;
//            inicilizeStartSett(&board, &ball, &player1, &player2);
//            counter=0;
//            printf("Player1: %d, Player2: %d\n", player1.score, player2.score);
//        }
//
//        moveBall(&ball);
//
//        if (counter == 0) {
//            up = true;
//        }
//        if (counter == 26) {
//            up = false;
//        }
//        if(counter%2==0){
//            if (up) {
//                playerMoveDown(&player1);
//                counter++;
//            } else {
//                playerMoveUp(&player1);
//                counter--;
//            }
//        }
//        else{
//            counter++;
//        }
//
//
//
//        renderAll(&board, ball, player1, player2);
//        printBoard(board);
//        newRound = checkBall(&ball, &player1, &player2);
//        usleep(1000*300);
//    }




    int counter = 0;
    uint32_t rgb_knobs_value;
    int  player1Position,  player2Position, player1LastPos, player2LastPos;
    int i,j;
    unsigned int c;
    unsigned char *mem_base;
    unsigned char *parlcd_mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL)
        exit(1);
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);

    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = (int32_t)0;
    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = (int32_t)0;
    rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    player2Position = (rgb_knobs_value & 0xff) / 4;
    player1Position = ((rgb_knobs_value >> 16) & 0xff) / 4;
    player1LastPos = player1Position;
    player2LastPos = player2Position;


    parlcd_hx8357_init(parlcd_mem_base);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    int newRound = 0;
    int winner;
    while (1){
        rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
        player2Position = (rgb_knobs_value & 0xff) / 4; // blue button
        player1Position = ((rgb_knobs_value >> 16) & 0xff) / 4;   // red button
        if (newRound == 1 || newRound == 2){
            newRound = 0;
            initializeStartSett(&board, &ball, &player1, &player2);
        }

        player1LastPos = movePlayer1(player1Position, player1LastPos, &player1);
        player2LastPos = movePlayer2(player2Position, player2LastPos, &player2);
        if(counter%3 == 0){
            moveBall(&ball);
        }
        if(counter >= 1000000){
            counter = 0;
        }
        newRound = checkBall(&ball, &player1, &player2);
        if(newRound == 1 || newRound == 2){
            if(player1.score >= board.maxScore|| player2.score >= board.maxScore){
                winner = player1.score > player2.score ? 1:2;
                break;
            }   else{
                goal(newRound, player1.score, player2.score, mem_base, parlcd_mem_base);
            }
        }

        renderAll(&board, ball, player1, player2);


        for (i = 0; i < 320 ; i++) {
            for (j = 0; j < 480 ; j++) {
                if(board.board[i/10][j/10] == 'T' || board.board[i/10][j/10] == 'R' || board.board[i/10][j/10] == 'L' || board.board[i/10][j/10] == 'B' || board.board[i/10][j/10] == 'X' || board.board[i/10][j/10] == '0'){
                    c = 0xFFFF;
                    parlcd_write_data(parlcd_mem_base, c);
                }   else{
                    c = 0x00;
                    parlcd_write_data(parlcd_mem_base, c);
                }
            }
        }
        counter++;
        usleep(1000);
    }
    end(winner, parlcd_mem_base, mem_base);
    return 0;

}


/**
 * This method is used to move player 1 when an interaction with the knobs is detected.
 * @param playerPosition
 * @param playerLastPosition
 * @param p
 * @return
 */
int movePlayer1(int playerPosition, int playerLastPosition, Player* p){
    if(playerLastPosition == 0){
        if(playerPosition == 63){
            playerMoveUp(p);
        }
    }   else if(playerLastPosition == 63){
        if(playerPosition == 0){
            playerMoveDown(p);
        }
    }   else{
        if(playerPosition<playerLastPosition){
            playerMoveUp(p);
        }   else if(playerPosition>playerLastPosition){
            playerMoveDown(p);
        }
    }
    return playerPosition;
}

/**
 * This method is used to move player 2 when an interaction with the knobs is detected.
 * @param playerPosition
 * @param playerLastPosition
 * @param p
 * @return
 */
int movePlayer2(int playerPosition, int playerLastPosition, Player* p){
    if(playerLastPosition == 0){
        if(playerPosition == 63){
            playerMoveDown(p);
        }
    }   else if(playerLastPosition == 63){
        if(playerPosition == 0){
            playerMoveUp(p);
        }
    }   else{
        if(playerPosition<playerLastPosition){
            playerMoveDown(p);
        }   else if(playerPosition>playerLastPosition){
            playerMoveUp(p);
        }
    }
    return playerPosition;
}

/**
 * This method is used to render a separate game background - the walls.
 * @param b
 */
void renderBoard(Board* b){
    for(int i = 0; i < b->height; i++){
        for(int j = 0; j < b->width; j++){
            if(i == 0){
                b->board[i][j] = 'T';
            }
            else if(i == b->height - 1){
                b->board[i][j] = 'B';
            }
            else if(j == 0){
                b->board[i][j] = 'L';
            }
            else if(j == b->width - 1){
                b->board[i][j] = 'R';
            }
            else{
                b->board[i][j] = ' ';
            }
        }
    }
}

/**
 * This method is used to render the player according to his position.
 * @param b
 * @param p
 */
void renderPlayer(Board *b, Player p){
    for(int i = 0; i < PLAYER_HEIGHT; i++){
        for(int j = 0; j < PLAYER_WIDTH; j++){
            b->board[p.positionY + i][p.positionX + j] = '0';
        }
    }
}

/**
 * This method is used to render the ball according to its position.
 * @param b
 * @param ball
 */
void renderBall(Board *b, Ball ball){
    for(int i = 0; i < BALL_WIDTH; i++){
        for(int j = 0; j < BALL_WIDTH; j++){
            b->board[ball.positionY + i][ball.positionX + j] = 'X';
        }
    }
}

/**
 * This method is used to run all renders via one function.
 * @param board
 * @param ball
 * @param player1
 * @param player2
 */
void renderAll(Board *board, Ball ball, Player player1, Player player2){
    renderBoard(board);
    renderPlayer(board, player1);
    renderPlayer(board, player2);
    renderBall(board, ball);
}

/**
 * This method is used to move the player up.
 * @param p
 */
void playerMoveUp(Player* p){
    if(p->positionY-1 >= 1){
        p->positionY -= 1;
    }
}

/**
 * This method is used to move the player down.
 * @param p
 */
void playerMoveDown(Player* p){
    if((p->positionY+1+PLAYER_HEIGHT) <= ROWS-1){
        p->positionY += 1;
    }
}

/**
 * This method is used for the picking the first vector of the ball on the start of any round.
 * @param ball
 */
void pickFirstVector(Ball* ball){
    int randomNumber = rand();
    if(randomNumber%2==0){
        ball->vectorX = 2;
        ball->vectorY = 0;
    }   else{
        ball->vectorX = -2;
        ball->vectorY = 0;
    }
    ball->counterOfVectorY = 0;
}

/**
 * This method moves the ball to the directions of the ball`s vectors.
 * @param ball
 */
void moveBall(Ball* ball){
    ball->positionX += ball->vectorX/2; //1 pixel per 1 cycle
    if(ball->vectorY == 1 || ball->vectorY == -1){ //1 pixel per 2 cycles
        if(ball->counterOfVectorY%2 == 0){
            ball->positionY += ball->vectorY;
            ball->counterOfVectorY += 1;
        }
        else{
            ball->counterOfVectorY += 1;
        }
    }
    else{
        ball->positionY += ball->vectorY/2; //1 pixel per 1 cycle
    }
}

/**
 * This method checks the position of the ball to see if it is in a restricted area - the wall, players, the goal zone.
 * If it is, it either performs a vector switch or adds a point to the player's score and triggers a new round.
 * @param ball
 * @param player1
 * @param player2
 * @return
 */
int checkBall(Ball* ball, Player* player1, Player* player2){
    //COLLISION WITH PLAYER

    if((ball->positionX == player1->positionX+1  && ball->positionY == player1->positionY+2) || (ball->positionX == player2->positionX-1 && ball->positionY == player2->positionY+2)){
        ball->vectorY = 0;
        ball->vectorX = -ball->vectorX;
        return 0;
    }
    if((ball->positionX == player1->positionX+1  && ball->positionY == player1->positionY+1) || (ball->positionX == player2->positionX-1  && ball->positionY == player2->positionY+1)){
        ball->vectorY = -1;
        ball->vectorX = -ball->vectorX;
        return 0;
    }
    if((ball->positionX == player1->positionX+1  && ball->positionY == player1->positionY+3) || (ball->positionX == player2->positionX-1  && ball->positionY == player2->positionY+3)){
        ball->vectorY = 1;
        ball->vectorX = -ball->vectorX;
        return 0;
    }
    if((ball->positionX == player1->positionX+1  && ball->positionY == player1->positionY) || (ball->positionX == player2->positionX-1  && ball->positionY == player2->positionY)){
        ball->vectorY = -1;
        ball->vectorX = -ball->vectorX;
        return 0;
    }
    if((ball->positionX == player1->positionX+1  && ball->positionY == player1->positionY+4) || (ball->positionX == player2->positionX-1  && ball->positionY == player2->positionY+4)){
        ball->vectorY = 1;
        ball->vectorX = -ball->vectorX;
        return 0;
    }

    //COLLISION WITH WALLS
    if((ball->positionX+ball->vectorX/2) <=0){
        player2->score +=1;
        return 2;
    }
    if((ball->positionX+ball->vectorX/2) >=COLS-1){
        player1->score+=1;
        return 1;
    }
    if((ball->positionY+ball->vectorY) <=0 || (ball->positionY+ball->vectorY) >=ROWS-1){
        ball->vectorY = -ball->vectorY;
        return 0;
    }
    return 0;
}

/**
 * This method is used at the beginning of each round to reset the basic positions and settings for the ball and players.
 * @param board
 * @param ball
 * @param player1
 * @param player2
 */
void initializeStartSett(Board* board, Ball* ball, Player* player1, Player* player2){
    ball->positionX = (int)(COLS/2-(int)(BALL_WIDTH/2));
    ball->positionY = (int)(ROWS/2-(int)(BALL_WIDTH/2));
    player1->positionX = 2;
    player1->positionY = 2;
    player2->positionX = COLS - 2 - PLAYER_WIDTH;
    player2->positionY = ROWS - 2 - PLAYER_HEIGHT;
    board->height = ROWS;
    board->width = COLS;
    pickFirstVector(ball);

}
